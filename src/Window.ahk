#Requires AutoHotKey v2.0

#Include events\EventTarget.ahk

class Window extends EventTarget
{
    static Events := {
        Show: 1, Hide: 2,
        Minimize: 3, Maximize: 4, Restore: 5,
        SizeChanged: 6, Moved: 7,
        Close: 8
    }

    Title
    {
        get => this._gui.Title
        set => this._gui.Title := Value
    }

    Left
    {
        get
        {
            WinGetPos(&x,,,, "ahk_id " this._gui.Hwnd)
            return x
        }
        set => this._gui.Move(Value)
    }

    Top
    {
        get
        {
            WinGetPos(,&y,,, "ahk_id " this._gui.Hwnd)
            return y
        }
        set => this._gui.Move(,Value)
    }

    Width
    {
        get
        {
            WinGetPos(,,&width,, "ahk_id " this._gui.Hwnd)
            return width
        }
        set => this._gui.Move(,,Value)
    }

    Height
    {
        get
        {
            WinGetPos(,,,&height, "ahk_id " this._gui.Hwnd)
            return height
        }
        set => this._gui.Move(,,,Value)
    }

    _owner := 0
    Owner
    {
        get => this._owner
        set
        {
            this._owner := Value
            this._gui.Opt("+Owner" Value)
        }
    }

    _parent := 0
    Parent
    {
        get => this._parent
        set
        {
            this._parent := Value
            this._gui.Opt("+Parent" Value)
        }
    }

    __New(args*) {
        this._gui := Gui(args*)
    }

    AddControl(args*)
    {
        return this._gui.Add(args*)
    }

    Show()
    {
        this._gui.Show()
        this.DispatchEvent(Window.Events.Show, EventArgs.Empty)
    }

    Hide()
    {
        this._gui.Hide()
        this.DispatchEvent(Window.Events.Hide, EventArgs.Empty)
    }

    Close()
    {
        this._gui.Close()
        this.DispatchEvent(Window.Events.Close, EventArgs.Empty)
    }
    
    Maximize()
    {
        this._gui.Maximize()
        this.DispatchEvent(Window.Events.Maximize, EventArgs.Empty)
    }

    Minimize()
    {
        this._gui.Minimize()
        this.DispatchEvent(Window.Events.Minimize, EventArgs.Empty)
    }

    Restore()
    {
        this._gui.Restore()
        this.DispatchEvent(Window.Events.Restore, EventArgs.Empty)
    }

    Move(left, top)
    {
        this.DispatchEvent(Window.Events.Moved, EventArgs.Empty)
        this._gui.Move(left, top,,)
    }

    Resize(width, height)
    {
        this._gui.Move(,, width, height)
        this.DispatchEvent(Window.Events.SizeChanged, EventArgs.Empty)
    }
}