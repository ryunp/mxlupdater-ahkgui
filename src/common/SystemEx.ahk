#Requires AutoHotKey v2.0

UriJoin(head, tails*)
{
    for tail in tails
        head := Format("{}\{}", RTrim(head, "\"), tail)
    
    return head
}

FileGetModifiedYear(file)
{
    return Integer(FormatTime(FileGetTime(file, "M"), "yyyy"))
}

DirGetFiles(dir, fileFilter := "*")
{
    files := Array()
    
    loop files UriJoin(dir, fileFilter)
    {
        files.Push(A_LoopFileFullPath)
    }

    return files
}