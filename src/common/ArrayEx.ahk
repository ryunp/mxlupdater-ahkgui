#Requires AutoHotkey v2.0

ArrayJoin(delim, head, tails*)
{
    for tail in tails
        head .= Format("{}{}", delim, tail)
    return head
}

ArrayReverse(array)
{
    reversed := Array()
    idx := 0
    Loop array.Length
        reversed.Push(array.Length - idx++)
    return reversed
}