#Requires AutoHotkey v2.0

#Include ..\common\ArrayEx.ahk
#Include ..\common\GuiEx.ahk
#Include ..\events\NotifyPropertyChanged.ahk
#Include ..\events\PresenterPropertyBindings.ahk
#Include ..\TextAnimation.ahk
#Include ..\Window.ahk

class UpdateWindow extends Window
{
    __New(Presenter) {
        super.__New("","MedianXL Updater")
        this.Presenter := Presenter
        this.PresenterProperty := PresenterPropertyBindings(Presenter)

        this.AddControl("Text", "h20 0x200", "Install Directory:")
        this.InstallDirEdit := this.AddControl("Edit", "yp w250", this.Presenter.InstallDir)
        this.InstallDirEdit.OnEvent("Change", (ctrl, info) => this.Presenter.InstallDir := this.InstallDirEdit.Text)
        this.PresenterProperty.AddChangeListener("InstallDir", value => this.InstallDirEdit.Text != value ? this.InstallDirEdit.Text := value : 0)
        this.PresenterProperty.AddChangeListener("IsUpdating", value => this.InstallDirEdit.Opt(Format("{}Disabled", value ? "+" : "-")))

        this.SelectInstallDirBtn := this.AddControl("Button", "yp", "Select")
        this.SelectInstallDirBtn.OnEvent("Click", (ctrl, info) => this.Presenter.SelectInstallDirDialog())
        this.PresenterProperty.AddChangeListener("IsUpdating", value => this.SelectInstallDirBtn.Opt(Format("{}Disabled", value ? "+" : "-")))

        this.LogDisplayEdit := this.AddControl("Edit", "xm w400 r12 ReadOnly", "")
        this.LogDisplayEdit.OnEvent("Change", (ctrl, info) => ControlScrollToBottom(this.LogDisplayEdit.Hwnd))
        this.PresenterProperty.AddChangeListener("EventLogAsString", value => this.UpdateLogDisplay(value))
        
        updateBtnOpts := Format("{}Disabled", this.Presenter.IsValidInstallDir ? "-" : "+")
        this.UpdateFilesBtn := this.AddControl("Button", updateBtnOpts, "Update")
        this.UpdateFilesBtn.OnEvent("Click", (ctrl, info) => this.Presenter.ExecuteUpdateMxl())
        this.PresenterProperty.AddChangeListener("IsValidInstallDir", value => this.UpdateFilesBtn.Opt(Format("{}Disabled", value ? "-" : "+")))
        this.PresenterProperty.AddChangeListener("IsUpdating", value => this.UpdateFilesBtn.Opt(Format("{}Disabled", value ? "+" : "-")))
        
        this.ProgressTxt := this.AddControl("Text", "yp w30 h20 center", "")
        this.ProgressTxt.SetFont("s12", "Consolas")
        this.ProgressTxt.Visible := false
        this.PresenterProperty.AddChangeListener("ProgressText", value => this.ProgressTxt.Text := value)
        this.PresenterProperty.AddChangeListener("IsUpdating", value => this.ProgressTxt.Visible := value)
    }

    UpdateLogDisplay(text)
    {
        this.LogDisplayEdit.Text := text
        ControlScrollToBottom(this.LogDisplayEdit.Hwnd)
    }
}
