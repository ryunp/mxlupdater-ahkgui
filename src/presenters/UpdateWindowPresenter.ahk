#Requires AutoHotKey v2.0

#Include ..\events\NotifyPropertyChanged.ahk

class UpdateWindowPresenter extends NotifyPropertyChanged
{
    IsUpdating => this.MxlClient.IsUpdating
    IsValidInstallDir => DirExist(this.InstallDir) ? true : false
    EventLogAsString => ArrayJoin("`r`n", this.Logger.GetAll()*)

    _progressText := ""
    ProgressText
    {
        get => this._progressText
        set => this.SetProperty('ProgressText', '_progressText', Value)
    }

    InstallDir
    {
        get => this.MxlClient.InstallDir
        set
        {
            this.MxlClient.InstallDir := Value
            this.RaisePropertyChanged("IsValidInstallDir")
        }
    }

    __New(Logger, MxlClient)
    {
        this.Logger := logger
        this.MxlClient := MxlClient
        this.ProgressAnim := TextAnimation(Array("——", "\", "|", "/"), 100)

        this.ProgressAnim.AddEventListener(TextAnimationEvents.Change, (s, args) => this.ProgressText := args.Text)

        this.MxlClient.AddEventListener(MedianXl.Events.InstallDirChange, (s, args) => this.RaisePropertyChanged("InstallDir"))
        this.MxlClient.AddEventListener(MedianXl.Events.UpdateBegin, ObjBindMethod(this, "MxlUpdateBeginHandler"))
        this.MxlClient.AddEventListener(MedianXl.Events.UpdateEnd, ObjBindMethod(this, "MxlUpdateEndHandler"))

        this.Logger.AddEventListener(MemoryLogger.Events.Entry, (s, args) => this.RaisePropertyChanged('EventLogAsString'))
    }

    SelectInstallDirDialog()
    {
        this.InstallDir := FileSelect("D", this.InstallDir)
    }

    MxlUpdateBeginHandler(sender, args)
    {
        this.RaisePropertyChanged("IsUpdating")
        this.ProgressAnim.Start()
    }

    MxlUpdateEndHandler(sender, args)
    {
        this.RaisePropertyChanged("IsUpdating")
        this.ProgressAnim.Stop()
    }

    ExecuteUpdateMxl()
    {
        this.MxlClient.Update()
    }
}