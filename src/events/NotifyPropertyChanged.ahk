#Requires AutoHotKey v2.0

#Include ..\events\EventTarget.ahk
#Include ..\events\EventArgs.ahk

; See: https://learn.microsoft.com/en-us/dotnet/api/system.componentmodel.inotifypropertychanged
class NotifyPropertyChanged extends EventTarget
{
    static Events := {
        PropertyChanged: 1
    }

    SetProperty(propName, backingPropName, value)
    {
        if (this.%backingPropName% != value)
        {
            this.%backingPropName% := value
            this.RaisePropertyChanged(propName)
            return true
        }

        return false
    }

    RaisePropertyChanged(propName := "")  
    {  
        this.DispatchEvent(NotifyPropertyChanged.Events.PropertyChanged, PropertyChangedEventArgs(propName))
    } 
}

class PropertyChangedEventArgs extends EventArgs
{
    __New(propName)
    {
        this.PropertyName := propName
    }
}