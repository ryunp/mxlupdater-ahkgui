#Requires AutoHotKey v2.0

; See: https://martinfowler.com/eaaDev/PresentationModel.html
class PresenterPropertyBindings
{
    Bindings := Map()

    __New(Presenter) {
        this.Presenter := Presenter
        this.Presenter.AddEventListener(NotifyPropertyChanged.Events.PropertyChanged, ObjBindMethod(this, "PresenterPropertyChangeHandler"))
    }

    PresenterPropertyChangeHandler(sender, args)
    {
        for action in this.Bindings.Get(args.PropertyName)
            action(this.Presenter.%args.PropertyName%)
    }

    AddChangeListener(propName, action)
    {
        if not this.Bindings.Has(propName)
            this.Bindings.Set(propName, Array())

        this.Bindings.Get(propName).Push(action)
    }

    RemoveChangeListener(propName, action)
    {
        for idx, storedAction in this.Bindings.Get(propName)
            if action = storedAction
            {
                this.Bindings.Get(propName).RemoveAt(idx)
                break
            }
    }
}