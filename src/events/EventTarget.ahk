#Requires AutoHotkey v2.0

; See: https://developer.mozilla.org/en-US/docs/Web/API/EventTarget
class EventTarget
{
    static Events := {}
    EventListeners := Map()

    ; Returns a function to remove the added EventHandler
    AddEventListener(eventName, callback)
    {
        if not this.EventListeners.Has(eventName)
            this.EventListeners.Set(eventName, Array())
        this.EventListeners.Get(eventName).Push(callback)
        return this.RemoveEventListener.Bind(this, eventName, callback)
    }

    RemoveEventListener(eventName, callback)
    {
        if this.EventListeners.Has(eventName)
            for fn in this.EventListeners.Get(eventName)
                if fn = callback
                {
                    this.EventListeners.RemoveAt(A_Index)
                    break
                }
    }

    DispatchEvent(eventName, args)
    {
        if this.EventListeners.Has(eventName)
            for fn in this.EventListeners.Get(eventName)
                fn(this, args)
    }
}