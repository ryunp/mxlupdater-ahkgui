#Requires AutoHotkey v2.0

; See: https://learn.microsoft.com/en-us/dotnet/api/system.threading.cancellationtokensource
class CancellationSource
{
    _isCancellationRequested := false
    _tokens := Array()

    IsCancellationRequested => this._isCancellationRequested
    Token => this._createToken()

    Cancel()
    {
        this._isCancellationRequested := true
        for token in this._tokens
            token._RaiseCancel()
    }

    CancelAfter(ms)
    {
        SetTimer(this.Cancel.Bind(this), ms)
    }

    _createToken()
    {
        cxToken := CancellationToken()
        this._tokens.Push(cxToken)
        return cxToken
    }
}

class CancellationToken
{
    _isCancellationRequested := false
    _cancelCallbacks := Array()

    IsCancellationRequested => this._isCancellationRequested

    Register(fn)
    {
        this._cancelCallbacks.Push(fn)
    }

    _RaiseCancel()
    {
        this._isCancellationRequested := true
        for fn in this._cancelCallbacks
            fn()
    }
}