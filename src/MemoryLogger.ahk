#Requires AutoHotKey v2.0

#Include events\EventTarget.ahk
#Include events\EventArgs.ahk

class MemoryLogger extends EventTarget
{
    static Events := {
        Entry: 1
    }

    Logs := Array()

    GetAll()
    {
        return this.Logs.Clone()
    }

    Add(message)
    {
        this.Logs.Push(message)
        this.DispatchEvent(MemoryLogger.Events.Entry, EventArgs.Empty)
    }
}