#Requires AutoHotkey v2.0

#Include ..\common\SystemEx.ahk
#Include ..\events\EventTarget.ahk

class SevenZip extends EventTarget
{
    static CmdLineFileName := "7zr.exe"
    static Url := Format("https://www.7-zip.org/a/{}", SevenZip.CmdLineFileName)
    
    IsInstalled => FileExist(UriJoin(this.InstallDir, SevenZip.CmdLineFileName))

    __New(installDir := A_Temp)
    {
        this.InstallDir := installDir
    }

    Install()
    {
        if this.IsInstalled
            return

        Download(SevenZip.url, UriJoin(this.InstallDir, SevenZip.CmdLineFileName))
    }

    Extract(archiveFile, destDir, overWrite := false)
    {
        fileName := UriJoin(this.InstallDir, SevenZip.CmdLineFileName)
        args := Format('e {} -o"{}"', archiveFile, destDir)
        
        if overWrite
            args .= " -aoa"
        
        cmd := Format('"{}" {}', fileName, args)
        RunWait(cmd)
    }
}