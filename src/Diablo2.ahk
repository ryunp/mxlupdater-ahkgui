#Requires AutoHotKey v2.0

#Include common\ArrayEx.ahk
#Include common\SystemEx.ahk

class Diablo2
{
    static FileIsModified := Map(
        "Fog.dll", dir => FileGetModifiedYear(UriJoin(dir, "Fog.dll")) > 2010
    )

    static IsInstalled := dir => FileExist(UriJoin(dir, "DiabloII.exe"))
}
