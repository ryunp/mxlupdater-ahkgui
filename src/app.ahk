#Requires AutoHotkey v2.0

#Include MemoryLogger.ahk
#Include MedianXL.ahk
#Include views\UpdateWindow.ahk
#Include presenters\UpdateWindowPresenter.ahk

Logger := MemoryLogger()
MxlClient := MedianXl(A_Args.Has(1) ? A_Args.Get(1) : "")

; Configuring Median Xl events logging
MxlClient.AddEventListener(MedianXl.Events.UpdateBegin, (s, args) => Logger.Add(Format("==========`r`n{}`r`n{}`r`n==========", args.Datetime, args.TargetDir)))
MxlClient.AddEventListener(MedianXl.Events.UpdateProgress, (s, args) => Logger.Add(args.Message))
MxlClient.AddEventListener(MedianXl.Events.UpdateEnd, (s, args) => Logger.Add(""))
MxlClient.AddEventListener(MedianXl.Events.FileDownload, (s, args) => Logger.Add(Format("Downloading '{}'", args.FileName)))
MxlClient.AddEventListener(MedianXl.Events.FileBackup, MxlFileBackupHandler)
MxlFileBackupHandler(sender, args)
{
    srcNameExt := StrSplit(args.SourcePath, "\").Pop()
    destNameExt := StrSplit(args.DestPath, "\").Pop()
    Logger.Add(Format("Backing up '{}' -> '{}'", srcNameExt, destNameExt))
}

MxlClient.AddEventListener(MedianXl.Events.FileInstall, MxlFileInstallHandler)
MxlFileInstallHandler(sender, args)
{
    srcNameExt := StrSplit(args.SourcePath, "\").Pop()
    Logger.Add(Format("Installing '{}' ({:.2f}MB)", srcNameExt, args.Bytes / (1024 ** 2)))
}

; Configure Update window
MainWinPresenter := UpdateWindowPresenter(Logger, MxlClient)
MainWin := UpdateWindow(MainWinPresenter)

; Begin user interaction
MainWin.Show()