#Requires AutoHotKey v2.0

#Include events\EventTarget.ahk
#Include events\EventArgs.ahk

class TextAnimation extends EventTarget
{
    ; Zero based index in order to work with remainders of Mod()
    CurrentFrameIdx := 0
    IsRunning := false

    __New(animFrames, intervalMs) {
        this.AnimationFrames := animFrames
        this.IntervalMs := intervalMs
        this.TickActionFn := this._tickAction.Bind(this)
    }

    Start()
    {
        if this.IsRunning
            return

        SetTimer(this.TickActionFn, this.IntervalMs)
        this.IsRunning := true
        this.DispatchEvent(TextAnimationEvents.Start, EventArgs.Empty)
    }

    Stop()
    {
        if not this.IsRunning
            return

        SetTimer(this.TickActionFn, 0)
        this.IsRunning := false
        this.CurrentFrameIdx := 0
        this.DispatchEvent(TextAnimationEvents.Stop, EventArgs.Empty)
    }

    _tickAction()
    {
        this.CurrentFrameIdx := Mod(this.CurrentFrameIdx, this.AnimationFrames.Length)
        this.DispatchEvent(
            TextAnimationEvents.Change,
            TextAnimationChangeEventArgs(this.AnimationFrames.Get(this.CurrentFrameIdx + 1))
        )

        this.CurrentFrameIdx++
    }
}

class TextAnimationEvents
{
    static Start := 1
    static Change := 2
    static Stop := 3
}

class TextAnimationChangeEventArgs extends EventArgs
{
    __New(text) {
        this.Text := text
    }
}