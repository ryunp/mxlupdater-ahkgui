#Requires AutoHotkey v2.0

#Include common\SystemEx.ahk
#Include events\EventTarget.ahk
#Include events\EventArgs.ahk
#Include vendor\SevenZip.ahk
#Include Diablo2.ahk

class MedianXl extends EventTarget
{
    static Events := {
        InstallDirChange: 1,
        UpdateBegin: 2, UpdateProgress: 3, UpdateEnd: 4,
        FileDownload: 5, FileBackup: 6, FileInstall: 7
    }

    IsUpdating := false
    IsInstalled => FileExist(UriJoin(this.InstallDir, "MXL.mpq"))
    
    _installDir := ""
    InstallDir {
        get => this._installDir
        set {
            if (Value != this._installDir)
            {
                oldValue := this._installDir
                this._installDir := Value
                this.DispatchEvent(MedianXl.Events.InstallDirChange, InstallDirChangeEventArgs(oldValue, Value))
            }
        }
    }

    __New(installDir := "") {
        this.InstallDir := installDir
    }

    Update()
    {
        if (not DirExist(this.InstallDir)) or this.IsUpdating
            return

        this.IsUpdating := true
        this.DispatchEvent(MedianXl.Events.UpdateBegin, MedianXlUpdateBeginEventArgs(FormatTime(), this.InstallDir))

        try
        {
            stagingDir := UriJoin(A_Temp, "mxl_update")
            stagingFilesPath := UriJoin(stagingDir, "*")

            ; Ensure staging dir exists
            DirCreate(stagingDir)

            ; Download latest files
            this.DispatchEvent(MedianXl.Events.UpdateProgress, MedianXlUpdateProgressEventArgs("Downloading files..."))
            this.DownloadServerFiles(stagingDir)

            ; Backup original or modded files
            this.DispatchEvent(MedianXl.Events.UpdateProgress, MedianXlUpdateProgressEventArgs("Backing up files..."))
            this.LoopBackupInstallDirFiles(stagingFilesPath)
            
            ; Move from staging area to install directory
            this.DispatchEvent(MedianXl.Events.UpdateProgress, MedianXlUpdateProgressEventArgs("Installing files..."))
            this.LoopMoveFiles(stagingFilesPath, this.InstallDir)

            this.DispatchEvent(MedianXl.Events.UpdateProgress, MedianXlUpdateProgressEventArgs("Update Complete!"))
        }
        finally
        {
            this.IsUpdating := false
            this.DispatchEvent(MedianXl.Events.UpdateEnd, EventArgs.Empty)
        }
    }

    LoopMoveFiles(srcDir, destDir)
    {
        Loop Files srcDir
        {
            destFilePath := UriJoin(destDir, A_LoopFileName)

            this.DispatchEvent(
                MedianXl.Events.FileInstall,
                MedianXlFileInstallEventArgs(A_LoopFileFullPath, destFilePath, FileGetSize(A_LoopFileFullPath))
            )

            FileMove(A_LoopFileFullPath, destFilePath, false)
        }
    }

    DownloadServerFiles(destDir)
    {
        static ServerArchiveFiles := Map(
            "mxl_dlls", "http://get.median-xl.com/launcher/?get=dlls",
            "mxl_mpq", "http://get.median-xl.com/launcher/?get=mod"
        )
        downloadedFiles := Array()

        ; Download archive files
        for fileName, url in ServerArchiveFiles
        {
            fileNameExt := Format("{}.7z", fileName)
            filePath := UriJoin(A_Temp, fileNameExt)

            this.DispatchEvent(MedianXl.Events.FileDownload, MedianXlFileDownloadEventArgs(fileNameExt))
            Download(url, filePath)
            downloadedFiles.Push(filePath)
        }

        ; Ensure 7zip is installed
        sZip := SevenZip()
        if not sZip.IsInstalled
        {
            this.DispatchEvent(MedianXl.Events.UpdateProgress, MedianXlUpdateProgressEventArgs("Installing 7zip..."))
            sZip.Install()
        }

        ; Extract archive files
        for filePath in downloadedFiles
        {
            fileNameExt := StrSplit(filePath, "\").Pop()
            this.DispatchEvent(MedianXl.Events.UpdateProgress, MedianXlUpdateProgressEventArgs(Format("Extracting {}...", fileNameExt)))
            sZip.Extract(filePath, destDir, true)
        }
    }

    LoopBackupInstallDirFiles(dirPath)
    {
        loop files dirPath
        {
            filePath := UriJoin(this.InstallDir, A_LoopFileName)

            if FileExist(filePath)
            {
                newFilePath := ""

                ; Determine installed file state
                if (Diablo2.FileIsModified.Has(A_LoopFileName) and not Diablo2.FileIsModified.Get(A_LoopFileName)(this.InstallDir))
                {
                    ; Rename original file to avoid being overwritten each update
                    newFilePath := Format("{}.d2_original", filePath)
                    MoveFileFnObj := FileMove.Bind(filePath, newFilePath, false)
                }
                else
                {
                    ; Overwrite previous backup
                    newFilePath := Format("{}.mxl_prev", filePath)
                    MoveFileFnObj := FileMove.Bind(filePath, newFilePath, true)
                }

                ; Execute
                this.DispatchEvent(
                    MedianXl.Events.FileBackup,
                    MedianXlFileBackupEventArgs(filePath, newFilePath, FileGetSize(filePath))
                )

                MoveFileFnObj()
            }
        }
    }
}

class InstallDirChangeEventArgs extends EventArgs
{
    __New(oldPath, newPath) {
        this.OldPath := oldPath
        this.NewPath := newPath
    }
}

class MedianXlUpdateBeginEventArgs extends EventArgs
{
    __New(datetime, targetDir) {
        this.Datetime := datetime
        this.TargetDir := targetDir
    }
}

class MedianXlUpdateProgressEventArgs extends EventArgs
{
    __New(message) {
        this.Message := message
    }
}

class MedianXlFileDownloadEventArgs extends EventArgs
{
    __New(fileName) {
        this.FileName := fileName
    }
}

class MedianXlFileBackupEventArgs extends EventArgs
{
    __New(sourcePath, destPath, bytes) {
        this.SourcePath := sourcePath
        this.DestPath := destPath
        this.Bytes := bytes
    }
}

class MedianXlFileInstallEventArgs extends EventArgs
{
    __New(sourcePath, destPath, bytes) {
        this.SourcePath := sourcePath
        this.DestPath := destPath
        this.Bytes := bytes
    }
}
