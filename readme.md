# MedianXL Updater - Ahk Gui
Testing basic AHK based Gui application for managing Median XL mod files.

This repo is dedicated to leveraging 'Tell, Dont Ask' logic design. Events for deferred execution control.
Goals:
- Always tell an object what needs to be accomplished
- Never 'ask' objects for data (implies understanding of enclosed behavior)
- Keep riggin of obejcts in main app file
- MVP style separation of user interface

Ahk v2 introduced better object encapsulation of the GUI domain, including basic Gui event management with the `OnEvent` method. Ahk doesn't offer any other generic Event infrastructure to work with. As the need for decoupling arises, concepts from well establish langauge GUI frameworks are slowly being integrated (mostly `.NET` and `JavaScript`).

Avoid unmanagable levels of coupling, always.

Basic updating GUI in action: https://youtu.be/cLThwGv_cns